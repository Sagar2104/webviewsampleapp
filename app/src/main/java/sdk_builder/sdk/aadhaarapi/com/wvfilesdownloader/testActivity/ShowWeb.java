package sdk_builder.sdk.aadhaarapi.com.wvfilesdownloader.testActivity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import sdk_builder.sdk.aadhaarapi.com.wvfilesdownloader.R;

public class ShowWeb extends Activity {
    //  public static Lesson L;

    WebView WV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WV = findViewById(R.id.web_view);
        WV.setWebViewClient(new WebViewClient() {
                                public void onLoadResource(WebView view, String url) {
                                    // Main.log(url);
                                    if (url.endsWith(".zip")) {
                                        finish();
                                    } else super.onLoadResource(view, url);
                                }
                            }
        );
        WV.loadUrl("https://eaadhaar.uidai.gov.in/#/");

    }
}