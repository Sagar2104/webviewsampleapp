//package sdk_builder.sdk.aadhaarapi.com.wvfilesdownloader;
//
//import im.delight.android.webview.AdvancedWebView;
//
//import android.util.Log;
//import android.webkit.WebChromeClient;
//import android.widget.Toast;
//import android.webkit.WebView;
//import android.view.View;
//import android.webkit.WebViewClient;
//import android.graphics.Bitmap;
//import android.content.Intent;
//import android.annotation.SuppressLint;
//import android.os.Bundle;
//import android.app.Activity;
//
//public class MainTestActivity extends Activity implements AdvancedWebView.Listener {
//
//    private static final String TEST_PAGE_URL = "https://eaadhaar.uidai.gov.in/#/";
//    private AdvancedWebView mWebView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        mWebView = (AdvancedWebView) findViewById(R.id.webview);
//        mWebView.setListener(this, this);
//        mWebView.setGeolocationEnabled(false);
//        mWebView.setMixedContentAllowed(true);
//        mWebView.setCookiesEnabled(true);
//        mWebView.setThirdPartyCookiesEnabled(true);
//        mWebView.setWebViewClient(new WebViewClient() {
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                Toast.makeText(MainTestActivity.this, "Finished loading", Toast.LENGTH_SHORT).show();
//                Log.i("Test: ", "Finished loading");
//
//
//            }
//
//        });
//        mWebView.setWebChromeClient(new WebChromeClient() {
//
//            @Override
//            public void onReceivedTitle(WebView view, String title) {
//                super.onReceivedTitle(view, title);
//                Toast.makeText(MainTestActivity.this, title, Toast.LENGTH_SHORT).show();
//                Log.i("Test: ", title);
//            }
//
//        });
//        mWebView.addHttpHeader("X-Requested-With", "");
//        mWebView.loadUrl(TEST_PAGE_URL);
//    }
//
//    @SuppressLint("NewApi")
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mWebView.onResume();
//        Log.i("Test: ", "onResume");
//        // ...
//    }
//
//    @SuppressLint("NewApi")
//    @Override
//    protected void onPause() {
//        mWebView.onPause();
//        // ...
//        super.onPause();
//        Log.i("Test: ", "onPAuse");
//    }
//
//    @Override
//    protected void onDestroy() {
//        mWebView.onDestroy();
//        // ...
//        Log.i("Test: ", "onDestroy");
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//        Log.i("Test: ", "onActResult: ");
//        mWebView.onActivityResult(requestCode, resultCode, intent);
//        // ...
//    }
//
//    @Override
//    public void onBackPressed() {
//        Log.i("Test: ", "onBackPressed");
//        if (!mWebView.onBackPressed()) { return; }
//        // ...
//        super.onBackPressed();
//    }
//
//    @Override
//    public void onPageStarted(String url, Bitmap favicon) {
//        Log.i("Test: ", "onPageStarted");
//        mWebView.setVisibility(View.INVISIBLE);
//    }
//
//    @Override
//    public void onPageFinished(String url) {
//
//        Log.i("Test: ", "onPageFinished");
//        mWebView.setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public void onPageError(int errorCode, String description, String failingUrl) {
//        Log.i("Test: ", "onPageError: (errorCode = "+errorCode+",  description = "+description+",  failingUrl = "+failingUrl+")");
//        Toast.makeText(MainTestActivity.this, "onPageError(errorCode = "+errorCode+",  description = "+description+",  failingUrl = "+failingUrl+")", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
//        Toast.makeText(MainTestActivity.this, "onDownloadRequested(url = "+url+",  suggestedFilename = "+suggestedFilename+",  mimeType = "+mimeType+",  contentLength = "+contentLength+",  contentDisposition = "+contentDisposition+",  userAgent = "+userAgent+")", Toast.LENGTH_LONG).show();
//        Log.i("Test: ", "onDownloadRequested(url = "+url+",  suggestedFilename = "+suggestedFilename+",  mimeType = "+mimeType+",  contentLength = "+contentLength+",  contentDisposition = "+contentDisposition+",  userAgent = "+userAgent+")");
//		if (AdvancedWebView.handleDownload(this, url, suggestedFilename)) {
//			// download successfully handled
//            Toast.makeText(MainTestActivity.this, "download successfully handled", Toast.LENGTH_LONG).show();
//            Log.i("Test: ", "download successfully handled");
//
//        }
//		else {
//			// download couldn't be handled because user has disabled download manager app on the device
//            Log.i("Test: ", "download couldn't be handled because user has disabled download manager app on the device");
//            Toast.makeText(MainTestActivity.this, "download couldn't be handled because user has disabled download manager app on the device", Toast.LENGTH_LONG).show();
//        }
//    }
//
//    @Override
//    public void onExternalPageRequest(String url) {
//        Log.i("Test: ", "onExternalPageRequest(url = "+url+")");
//        Toast.makeText(MainTestActivity.this, "onExternalPageRequest(url = "+url+")", Toast.LENGTH_SHORT).show();
//    }
//}
