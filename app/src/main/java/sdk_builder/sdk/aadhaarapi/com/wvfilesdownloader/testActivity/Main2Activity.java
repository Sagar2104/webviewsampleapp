package sdk_builder.sdk.aadhaarapi.com.wvfilesdownloader.testActivity;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.widget.Toast;

import java.io.File;

import im.delight.android.webview.AdvancedWebView;
import sdk_builder.sdk.aadhaarapi.com.wvfilesdownloader.R;

public class Main2Activity extends AppCompatActivity implements AdvancedWebView.Listener {
    private static final String QT_TAG = "QT_TAG";
    private AdvancedWebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2
        );

        mWebView = findViewById(R.id.webview);
        mWebView.setListener(this, this);
        String url = "https://resident.uidai.gov.in/offline-kyc";
        mWebView.loadUrl(url);

//         mWvOtp = findViewById(R.id.web_view);
//         mWebViewSettings = mWvOtp.getSettings();
//         initWebView();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType,
                                    long contentLength, String contentDisposition, String userAgent) {
        Log.i(QT_TAG, "downloadlistener0: "
                + userAgent + " " + mimeType + " " + contentLength + " "
                + contentDisposition + " " + url + "  " + suggestedFilename);

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

        request.setMimeType(mimeType);
        request.setDestinationUri(Uri.fromFile(new File(Environment.DIRECTORY_DOWNLOADS)));
        //------------------------COOKIE!!------------------------
        String cookies = CookieManager.getInstance().getCookie(url);
        request.addRequestHeader("cookie", cookies);
        //------------------------COOKIE!!------------------------
        request.addRequestHeader("User-Agent", userAgent);
        request.setDescription("Downloading file...");
        request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
        DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(request);
        Toast.makeText(getApplicationContext(), "Downloading File0 ", Toast.LENGTH_LONG).show();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        Log.i(QT_TAG, "onResume: ");
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        Log.i(QT_TAG, "onPause: ");
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        Log.i(QT_TAG, "onDestroy: ");
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent.getData() != null) {
            Log.i(QT_TAG, "onActivityResult: " + requestCode + " " + resultCode + " " + intent.getData().toString());
        } else {
            Log.i(QT_TAG, "onActivityResult: " + requestCode + " " + resultCode);
        }

        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) {
            Log.i(QT_TAG, "onBackPressed: ");
            return;
        }
        // ...
        super.onBackPressed();
    }


    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        Log.i(QT_TAG, "onPageStarted: " + url);
    }

    @Override
    public void onPageFinished(String url) {
        Log.i(QT_TAG, "onPageFinished: " + url);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        Log.i(QT_TAG, "onPageError " + errorCode + " " + description + " " + failingUrl);
    }

    @Override
    public void onExternalPageRequest(String url) {
        Log.i(QT_TAG, "onExternalPageRequest: " + url);

    }
}



/*
*
*
    private void initWebView() {
        WebView wv = findViewById(R.id.web_view);
        String url = "https://resident.uidai.gov.in/offline-kyc";

        mWebViewSettings.setJavaScriptEnabled(true);
        mWebViewSettings.setSupportMultipleWindows(true);
        mWebViewSettings.setAllowContentAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mWebViewSettings.setAllowUniversalAccessFromFileURLs(true);
        }

        mWebViewSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebViewSettings.setBuiltInZoomControls(false);
        mWebViewSettings.setJavaScriptEnabled(true);
        mWebViewSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebViewSettings.setAllowFileAccess(true);
        mWebViewSettings.setAllowContentAccess(true);
        mWebViewSettings.setAllowFileAccess(true);
        mWebViewSettings.setAllowFileAccessFromFileURLs(true);
        mWebViewSettings.setAllowUniversalAccessFromFileURLs(true);

        wv.loadUrl(url);

//        mWebViewSettings.setDomStorageEnabled(true);
//        mWebViewSettings.setBlockNetworkImage(true);
//        mWebViewSettings.setLoadsImagesAutomatically(true);
//        mWebViewSettings.setDisplayZoomControls(false);
//        mWebViewSettings.setBuiltInZoomControls(false);
//        mWebViewSettings.setSupportZoom(false);
//        mWebViewSettings.setUseWideViewPort(true);
//        mWebViewSettings.setLoadWithOverviewMode(true);
//        mWebViewSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

//        if (Build.VERSION.SDK_INT >= 23) {
//            mWebViewSettings.setOffscreenPreRaster(true);
//        }
//        if (Build.VERSION.SDK_INT >= 19) {
//            mWvOtp.setLayerType(2, null);
//        } else {
//            mWvOtp.setLayerType(1, null);
//        }

//        mWvOtp.loadUrl("https://resident.uidai.gov.in/offline-kyc");

       // mWvOtp.setScrollBarStyle(0);
  //      mWvOtp.setWebChromeClient(new MyWebChromeClient());
        mWvOtp.setWebViewClient(new MyWebViewClientEx());

      //  mWvOtp.addJavascriptInterface();
   //     mWvOtp.addJavascriptInterface(this, "android");
//        mWvOtp.setOnTouchListener(new View.OnTouchListener() {
//            public boolean onTouch(View v, MotionEvent ev) {
//                ((WebView) v).requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });

        mWvOtp.setDownloadListener(new DownloadListener() {
        @Override
        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
           String fileName =  URLUtil.guessFileName(url, contentDisposition, mimeType);

            Log.i(TAG, "downloadlistener0: " + userAgent +" " + mimeType+" " +  contentLength + " " + contentDisposition+ " " + url);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

            request.setMimeType(mimeType);
            //------------------------COOKIE!!------------------------
            String cookies = CookieManager.getInstance().getCookie(url);
            request.addRequestHeader("cookie", cookies);
            //------------------------COOKIE!!------------------------
            request.addRequestHeader("User-Agent", userAgent);
            request.setDescription("Downloading file...");
            request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            dm.enqueue(request);
            Toast.makeText(getApplicationContext(), "Downloading File0 ", Toast.LENGTH_LONG).show();
        }
    });
}

    private final class MyWebChromeClient extends WebChromeClient {
        private MyWebChromeClient() {
        }

        public void onProgressChanged(WebView view, final int newProgress) {
            super.onProgressChanged(view, newProgress);
            Log.i(TAG, "onProgressChanged, "+ "newProgress, " + Integer.valueOf(newProgress));
            runOnUiThread(new Runnable() {
                public void run() {
//                    if (this.mTvRetry.getVisibility() != 0) {
//                        DFOTPWebFragment.this.updateProgress(newProgress);
//                    }
                }
            });
        }
    }

    class MyWebViewClientEx extends WebViewClient {
        MyWebViewClientEx() {
        }

        private static final String QT_TAG = "QT_TAG";

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //url will be the url that you click in your webview.
            //you can open it with your own webview or do
            //whatever you want

            //Here is the example that you open it your own webview.

            Log.i(QT_TAG, "listener  overide url 1");
//
//            Log.i(QT_TAG, "listener on download start 1");
//            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//
//            request.setMimeType("application/zip");
//            //------------------------COOKIE!!------------------------
//            String cookies = CookieManager.getInstance().getCookie(url);
//            request.addRequestHeader("cookie", cookies);
//            //------------------------COOKIE!!------------------------
//            request.addRequestHeader("User-Agent", "Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36");
//            request.setDescription("Downloading file...");
//            request.setTitle(fileName);
//            request.allowScanningByMediaScanner();
//            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,fileName);
//            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
//            dm.enqueue(request);
          //  Toast.makeText(getApplicationContext(), "Downloading File1", Toast.LENGTH_LONG).show();
            view.loadUrl(url);
            return true;
        }
//
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            return super.shouldOverrideUrlLoading(view, url);
//        }

        public void onLoadResource(WebView view, String url) {
          //  DFKYCUtils.logI(DFOTPWebFragment.TAG, "MyWebViewClientEx", "onLoadResource", url);
            super.onLoadResource(view, url);
        }

        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
//            if (isLoadResource(url)) {
//                return super.shouldInterceptRequest(view, url);
//            }
           // DFKYCUtils.logI(DFOTPWebFragment.TAG, "MyWebViewClientEx", "2====shouldInterceptRequest", url);
            return new WebResourceResponse("", "", null);
        }

        public void onPageFinished(WebView view, String url) {
           // DFKYCUtils.logI(DFOTPWebFragment.TAG, "MyWebViewClientEx", "onPageFinished", url);
            super.onPageFinished(view, url);
        }
    }

    @JavascriptInterface
    public void jsCallAndroid(String args) {
       // DFKYCUtils.logI(TAG, "jsCallAndroid", args);
        Log.i(TAG, " Reached jsCallAndroid");
//        if (!TextUtils.isEmpty(args)) {
//            String[] split = args.split("@");
//            if (split.length >= 2) {
//                String otp = split[0];
//                String password = split[1];
//                if (otp.length() == 6 && password.length() == 4) {
//                    verifyOTPNumber(otp, password);
//                }
//            }
//        }
    }

    @JavascriptInterface
    public void clickInputOTP() {

        Log.i(TAG, " Reached clickInputOTP");
    }
* */