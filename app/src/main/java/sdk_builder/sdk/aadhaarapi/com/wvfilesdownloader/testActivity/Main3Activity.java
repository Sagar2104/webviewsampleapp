package sdk_builder.sdk.aadhaarapi.com.wvfilesdownloader.testActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.MimeTypeMap;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import sdk_builder.sdk.aadhaarapi.com.wvfilesdownloader.R;

public class Main3Activity extends AppCompatActivity {

    private static final int MY_PERMISSION_REQUEST_CODE = 123;
    String QT_TAG = "download test";

    String fileName = "test";
    String mimeType1 = "null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final WebView wv = findViewById(R.id.web_view);

        checkPermission();
        String url = "https://resident.uidai.gov.in/offline-kyc";
        wv.loadUrl(url);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setSupportMultipleWindows(true);
        wv.getSettings().setAllowContentAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
        }

        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.getSettings().setBuiltInZoomControls(false);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setAllowContentAccess(true);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setAllowFileAccessFromFileURLs(true);
        wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
        wv.getSettings().setDomStorageEnabled(true);

        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setPluginState(WebSettings.PluginState.ON);
        wv.getSettings().setSupportZoom(true);
        wv.getSettings().setAllowFileAccess(true);

        // userAgent:  downloadlistener: Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36
        // mimeType: application/zip
        // contentLength: 5445
        // contentDispostition: attachment; filename=offlineaadhaar20190925055612130.zip
        // url: https://resident.uidai.gov.in/offline-kyc

        wv.setWebViewClient(new WebViewClient() {
//
//            @Override
//            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
//             //   Log.i(QT_TAG, "listener intercept request");
//                try {
//                    String ext = MimeTypeMap.getFileExtensionFromUrl(url);
//                    String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext);
//
//                    Log.i(QT_TAG, "listener intercept request:: " + ext +" :: " + mime);
//
//                    if (mime == null) {
//                        Log.i(QT_TAG, "listener intercept request" +"mime == null ");
//                        if (mimeType1.equalsIgnoreCase("application/zip")){
//                            Log.i(QT_TAG, "listener intercept request" +"mime0 == application/zip ");
//
////                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
////                            request.setMimeType(mimeType1);
////                            String cookies = CookieManager.getInstance().getCookie(url);
////                            request.addRequestHeader("cookie", cookies);
////                            request.addRequestHeader("User-Agent", "Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36");
////                            request.setDescription("Downloading file...");
////                            request.setTitle(fileName);
////                            request.allowScanningByMediaScanner();
////                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
////                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,fileName);
////                            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
////                            dm.enqueue(request);
//                            Toast.makeText(getApplicationContext(), "Downloading File::", Toast.LENGTH_LONG).show();
//                        }else{
//                            return super.shouldInterceptRequest(view, url);
//                        }
//
//                        return super.shouldInterceptRequest(view, url);
//                       // view.loadUrl(url);
//                    } else {
//                        Log.i(QT_TAG, "listener intercept request" +"mime != null ");
//                        if (mimeType1 == "application/zip"){
//                            Log.i(QT_TAG, "listener intercept request" +"mime1 == application/zip ");
//
////                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
////                            request.setMimeType(mimeType1);
////                            String cookies = CookieManager.getInstance().getCookie(url);
////                            request.addRequestHeader("cookie", cookies);
////                            request.addRequestHeader("User-Agent", "Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36");
////                            request.setDescription("Downloading file...");
////                            request.setTitle(fileName);
////                            request.allowScanningByMediaScanner();
////                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
////                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,fileName);
////                            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
////                            dm.enqueue(request);
////                            Toast.makeText(getApplicationContext(), "Downloading File::", Toast.LENGTH_LONG).show();
//                        }else{
//                            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
//                            //  conn.setRequestProperty("User-Agent", userAgent);
//                            return new WebResourceResponse(mime, "UTF-8", conn.getInputStream());
//                        }
//
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                    Log.e(QT_TAG, "listener intercept request:" +" exception "+ e.getMessage());
//                }
//
//                return null;
//            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //url will be the url that you click in your webview.
                //you can open it with your own webview or do
                //whatever you want

                //Here is the example that you open it your own webview.
                Log.i(QT_TAG, "listener  overide url 1");
                Log.i(QT_TAG, "listener on download start 1");

                wv.loadUrl("javascript:(function(){" +
                        "l=document.getElementsByClassName('smt-totp')[0];" +
                        "e=document.createEvent('HTMLEvents');" +
                        "e.initEvent('onclick',true,true);" +
                        "l.dispatchEvent(e);" +
                        "})()");

                String js = "javascript:(function(){" +
                        "l=document.getElementsByClassName('smt-totp')[0];" +
                        "l.onclick;" +
                        "})()";

                String js2 = "javascript:(function(){" +
                        "l=document.getElementsByClassName('smt-totp')[0];" +
                        "e=document.createEvent('HTMLEvents');" +
                        "e.initEvent('click',true,true);" +
                        "l.dispatchEvent(e);" +
                        "})()";

                String js3 = "javascript:(function(){" +
                        "l=document.getElementsByClassName('smt-totp')[0];" +
                        "e=document.createEvent('HTMLEvents');" +
                        "e.initEvent('onclick',true,true);" +
                        "l.dispatchEvent(e);" +
                        "})()";

                wv.evaluateJavascript(js, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.i(QT_TAG, "listener on button press");
                        String result = s;
                        Log.i(QT_TAG, "listener on button press1 " + s);
                    }
                });

                wv.evaluateJavascript(js2, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.i(QT_TAG, "listener on button press2");
                        String result = s;
                        Log.i(QT_TAG, "listener on button press1=2 " + s);
                    }
                });

                wv.evaluateJavascript(js3, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.i(QT_TAG, "listener on button press3");
                        String result = s;
                        Log.i(QT_TAG, "listener on button press3 " + s);
                    }
                });


                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                request.setMimeType("application/zip");
                //------------------------COOKIE!!------------------------
                String cookies = CookieManager.getInstance().getCookie(url);
                request.addRequestHeader("cookie", cookies);
                //------------------------COOKIE!!------------------------
                request.addRequestHeader("User-Agent", "Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36");
                request.setDescription("Downloading file...");
                request.setTitle(fileName);
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
                Toast.makeText(getApplicationContext(), "Downloading File1", Toast.LENGTH_LONG).show();
                view.loadUrl(url);
                return true;
            }
        });

// <button type=“button” onclick=“” value=“Submit” class=“smt-totp btn btn-primary ripple r-width-300 m-5 button-icon”>
// <i class=“material-icons fs-15”>file_download</i> Download </button>

        wv.loadUrl("javascript:(function(){" +
                "l=document.getElementsByClassName('smt-totp')[0];" +
                "e=document.createEvent('HTMLEvents');" +
                "e.initEvent('click',true,true);" +
                "l.dispatchEvent(e);" +
                "})()");

        String js = "javascript:(function(){" +
                "l=document.getElementsByClassName('smt-totp')[0];" +
                "l.onclick;" +
                "})()";

        String js2 = "javascript:(function(){" +
                "l=document.getElementsByClassName('smt-totp')[0];" +
                "e=document.createEvent('HTMLEvents');" +
                "e.initEvent('click',true,true);" +
                "l.dispatchEvent(e);" +
                "})()";

        String js3 = "javascript:(function(){" +
                "l=document.getElementsByClassName('smt-totp')[0];" +
                "e=document.createEvent('HTMLEvents');" +
                "e.initEvent('onclick',true,true);" +
                "l.dispatchEvent(e);" +
                "})()";

        wv.evaluateJavascript(js, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                Log.i(QT_TAG, "listener on button press");
                String result = s;
                Log.i(QT_TAG, "listener on button press1 " + s);
            }
        });

        wv.evaluateJavascript(js2, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                Log.i(QT_TAG, "listener on button press2");
                String result = s;
                Log.i(QT_TAG, "listener on button press1=2 " + s);
            }
        });

        wv.evaluateJavascript(js3, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                Log.i(QT_TAG, "listener on button press3");
                String result = s;
                Log.i(QT_TAG, "listener on button press3 " + s);
            }
        });


        wv.setDownloadListener(new DownloadListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition,
                                        String mimetype, long contentLength) {

                Log.i(QT_TAG, "downloadlistener0: " + userAgent + " " + mimetype + " " +
                        contentLength + " " + contentDisposition + " " + url);

                wv.loadUrl("javascript:(function(){" +
                        "l=document.getElementsByClassName('smt-totp btn btn-primary ripple r-width-300 m-5 button-icon')[0];" +
                        "e=document.createEvent('HTMLEvents');" +
                        "e.initEvent('click',true,true);" +
                        "l.dispatchEvent(e);" +
                        "})()");

                String js = "javascript:(function(){" +
                        "l=document.getElementsByClassName('smt-totp')[0];" +
                        "l.onclick;" +
                        "})()";

                String js2 = "javascript:(function(){" +
                        "l=document.getElementsByClassName('smt-totp')[0];" +
                        "e=document.createEvent('HTMLEvents');" +
                        "e.initEvent('click',true,true);" +
                        "l.dispatchEvent(e);" +
                        "})()";

                String js3 = "javascript:(function(){" +
                        "l=document.getElementsByClassName('smt-totp')[0];" +
                        "e=document.createEvent('HTMLEvents');" +
                        "e.initEvent('onclick',true,true);" +
                        "l.dispatchEvent(e);" +
                        "})()";

                wv.evaluateJavascript(js, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.i(QT_TAG, "listener on button press");
                        String result = s;
                        Log.i(QT_TAG, "listener on button press1 " + s);
                    }
                });

                wv.evaluateJavascript(js2, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.i(QT_TAG, "listener on button press2");
                        String result = s;
                        Log.i(QT_TAG, "listener on button press1=2 " + s);
                    }
                });

                wv.evaluateJavascript(js3, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.i(QT_TAG, "listener on button press3");
                        String result = s;
                        Log.i(QT_TAG, "listener on button press3 " + s);
                    }
                });


                MimeTypeMap mtm = MimeTypeMap.getSingleton();
                DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Uri downloadUri = Uri.parse(url);

                Log.i(QT_TAG, "downloadlistener0.1: " + " " + mtm.toString() + " " + " " + downloadUri.toString());

                // get file name. if filename exists in contentDisposition, use it. otherwise, use the last part of the url.
                String fileName = downloadUri.getLastPathSegment();
                int pos = 0;

                if ((pos = contentDisposition.toLowerCase().lastIndexOf("filename=")) >= 0) {
                    fileName = contentDisposition.substring(pos + 9);
                    pos = fileName.lastIndexOf(";");
                    if (pos > 0) {
                        fileName = fileName.substring(0, pos - 1);
                    }
                }
                // predict MIME Type
                String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
                String mimeType = mtm.getMimeTypeFromExtension(fileExtension);
                fileName = fileName.replaceAll("\"", "");

                // request saving in Download directory
                Log.i(QT_TAG, "downloadlistener1: " + fileExtension + " " + mimeType + " " +
                        fileName + " " + contentDisposition + " " + downloadUri.toString());
                DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                request.setTitle(fileName);
                request.setDescription(url);
                request.setMimeType(mimeType);

                //  Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS).mkdirs();
                //  request.setDestinationUri(Uri.fromFile(new File(String.valueOf(Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS))));
                //------------------------COOKIE!!------------------------
                String cookies = CookieManager.getInstance().getCookie(url);
                request.addRequestHeader("cookie", cookies);
                //------------------------COOKIE!!------------------------
                request.addRequestHeader("User-Agent", userAgent);
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                // request in download manager
                downloadManager.enqueue(request);
            }
        });

        //              wv.addJavascriptInterface(this, "android");

/*        wv.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {


                fileName =  URLUtil.guessFileName(url, contentDisposition, mimeType);
                fileName=fileName.replaceAll("\"", "");
                mimeType1 = mimeType;

               Log.i(QT_TAG, "downloadlistener0: " + userAgent +" " + mimeType+" " +  contentLength + " " + contentDisposition+ " " + url);

//                Intent i = new Intent(Intent.);
//                i.setData(Uri.parse(url));
//                startActivity(i);

//                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//
//                request.setMimeType(mimeType);
//                request.setDestinationUri(Uri.fromFile(new File( Environment.DIRECTORY_DOWNLOADS)));
//                //------------------------COOKIE!!------------------------
//                String cookies = CookieManager.getInstance().getCookie(url);
//                request.addRequestHeader("cookie", cookies);
//                //------------------------COOKIE!!------------------------
//                request.addRequestHeader("User-Agent", userAgent);
//                request.setDescription("Downloading file...");
//                request.setTitle(fileName);
//                request.allowScanningByMediaScanner();
//                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
//                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
//                dm.enqueue(request);
                Toast.makeText(getApplicationContext(), "Downloading File0 ", Toast.LENGTH_LONG).show();
            }
        });*/
    }

    @JavascriptInterface
    public void jsCallAndroid(String args) {
        // DFKYCUtils.logI(TAG, "jsCallAndroid", args);
        Log.i(QT_TAG, " Reached jsCallAndroid");
//        if (!TextUtils.isEmpty(args)) {
//            String[] split = args.split("@");
//            if (split.length >= 2) {
//                String otp = split[0];
//                String password = split[1];
//                if (otp.length() == 6 && password.length() == 4) {
//                    verifyOTPNumber(otp, password);
//                }
//            }
//        }
    }

    @JavascriptInterface
    public void clickInputOTP() {

        Log.i(QT_TAG, " Reached clickInputOTP");
    }

    protected void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // show an alert dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(Main3Activity.this);
                    builder.setMessage("Write external storage permission is required.");
                    builder.setTitle("Please grant permission");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(
                                    Main3Activity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSION_REQUEST_CODE
                            );
                        }
                    });
                    builder.setNeutralButton("Cancel", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    // Request permission
                    ActivityCompat.requestPermissions(
                            Main3Activity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSION_REQUEST_CODE
                    );
                }
            } else {
                // Permission already granted
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission granted
                } else {
                    // Permission denied
                }
            }
        }
    }
}
