package sdk_builder.sdk.aadhaarapi.com.wvfilesdownloader;

import android.Manifest;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.File;

public class WebviewSampleActivity extends AppCompatActivity {

    private static final int MY_PERMISSION_REQUEST_CODE = 123;
    String QT_TAG = "webview_download_test";
    String fileName = "test";
    String mimeType1 = "null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final WebView wv = findViewById(R.id.web_view);

        checkPermission();
        String url = "https://resident.uidai.gov.in/offline-kyc";
        wv.loadUrl(url);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setSupportMultipleWindows(true);
        wv.getSettings().setAllowContentAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
        }

        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.getSettings().setBuiltInZoomControls(false);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setAllowContentAccess(true);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setAllowFileAccessFromFileURLs(true);
        wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
        wv.getSettings().setDomStorageEnabled(true);

        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setPluginState(WebSettings.PluginState.ON);
        wv.getSettings().setSupportZoom(true);
        wv.getSettings().setAllowFileAccess(true);

        // userAgent:  downloadlistener: Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36
        // mimeType: application/zip
        // contentLength: 5445
        // contentDispostition: attachment; filename=offlineaadhaar20190925055612130.zip
        // url: https://resident.uidai.gov.in/offline-kyc

        wv.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {


                fileName = URLUtil.guessFileName(url, contentDisposition, mimeType);
                fileName = fileName.replaceAll("\"", "");
                mimeType1 = mimeType;

                Log.i(QT_TAG, "downloadlistener0: " + userAgent + " " + mimeType + " " + contentLength + " " + contentDisposition + " " + url);

                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                request.setMimeType(mimeType);
                request.setDestinationUri(Uri.fromFile(new File(Environment.DIRECTORY_DOWNLOADS)));
                //------------------------COOKIE!!------------------------
                String cookies = CookieManager.getInstance().getCookie(url);
                request.addRequestHeader("cookie", cookies);
                //------------------------COOKIE!!------------------------
                request.addRequestHeader("User-Agent", userAgent);
                request.setDescription("Downloading file...");
                request.setTitle(fileName);
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
                Toast.makeText(getApplicationContext(), "Downloading File0 ", Toast.LENGTH_LONG).show();
            }
        });


        wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //url will be the url that you click in your webview.
                //you can open it with your own webview or do
                //whatever you want

                //Here is the example that you open it your own webview.
                Log.i(QT_TAG, "listener  overide url 1");
                Log.i(QT_TAG, "listener on download start 1");

                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                request.setMimeType("application/zip");
                //------------------------COOKIE!!------------------------
                String cookies = CookieManager.getInstance().getCookie(url);
                request.addRequestHeader("cookie", cookies);
                //------------------------COOKIE!!------------------------
                request.addRequestHeader("User-Agent", "Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36");
                request.setDescription("Downloading file...");
                request.setTitle(fileName);
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
                Toast.makeText(getApplicationContext(), "Downloading File1", Toast.LENGTH_LONG).show();
                view.loadUrl(url);
                return true;
            }
        });

// <button type=“button” onclick=“” value=“Submit” class=“smt-totp btn btn-primary ripple r-width-300 m-5 button-icon”>
// <i class=“material-icons fs-15”>file_download</i> Download </button>


    }

    protected void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // show an alert dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(WebviewSampleActivity.this);
                    builder.setMessage("Write external storage permission is required.");
                    builder.setTitle("Please grant permission");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(
                                    WebviewSampleActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSION_REQUEST_CODE
                            );
                        }
                    });
                    builder.setNeutralButton("Cancel", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    // Request permission
                    ActivityCompat.requestPermissions(
                            WebviewSampleActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSION_REQUEST_CODE
                    );
                }
            } else {
                // Permission already granted
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission granted
                } else {
                    // Permission denied
                }
            }
        }
    }
}


//
//            @Override
//            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
//             //   Log.i(QT_TAG, "listener intercept request");
//                try {
//                    String ext = MimeTypeMap.getFileExtensionFromUrl(url);
//                    String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext);
//
//                    Log.i(QT_TAG, "listener intercept request:: " + ext +" :: " + mime);
//
//                    if (mime == null) {
//                        Log.i(QT_TAG, "listener intercept request" +"mime == null ");
//                        if (mimeType1.equalsIgnoreCase("application/zip")){
//                            Log.i(QT_TAG, "listener intercept request" +"mime0 == application/zip ");
//
////                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
////                            request.setMimeType(mimeType1);
////                            String cookies = CookieManager.getInstance().getCookie(url);
////                            request.addRequestHeader("cookie", cookies);
////                            request.addRequestHeader("User-Agent", "Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36");
////                            request.setDescription("Downloading file...");
////                            request.setTitle(fileName);
////                            request.allowScanningByMediaScanner();
////                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
////                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,fileName);
////                            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
////                            dm.enqueue(request);
//                            Toast.makeText(getApplicationContext(), "Downloading File::", Toast.LENGTH_LONG).show();
//                        }else{
//                            return super.shouldInterceptRequest(view, url);
//                        }
//
//                        return super.shouldInterceptRequest(view, url);
//                       // view.loadUrl(url);
//                    } else {
//                        Log.i(QT_TAG, "listener intercept request" +"mime != null ");
//                        if (mimeType1 == "application/zip"){
//                            Log.i(QT_TAG, "listener intercept request" +"mime1 == application/zip ");
//
////                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
////                            request.setMimeType(mimeType1);
////                            String cookies = CookieManager.getInstance().getCookie(url);
////                            request.addRequestHeader("cookie", cookies);
////                            request.addRequestHeader("User-Agent", "Mozilla/5.0 (Linux; Android 7.1.2; Redmi Y1 Lite Build/N2G47H; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.132 Mobile Safari/537.36");
////                            request.setDescription("Downloading file...");
////                            request.setTitle(fileName);
////                            request.allowScanningByMediaScanner();
////                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
////                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,fileName);
////                            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
////                            dm.enqueue(request);
////                            Toast.makeText(getApplicationContext(), "Downloading File::", Toast.LENGTH_LONG).show();
//                        }else{
//                            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
//                            //  conn.setRequestProperty("User-Agent", userAgent);
//                            return new WebResourceResponse(mime, "UTF-8", conn.getInputStream());
//                        }
//
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                    Log.e(QT_TAG, "listener intercept request:" +" exception "+ e.getMessage());
//                }
//
//                return null;
//            }
